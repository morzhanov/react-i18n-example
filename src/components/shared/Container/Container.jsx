import React from 'react'
import styled from 'styled-components'
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl'
import { inject, observer } from 'mobx-react'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: calc(100% - 64px);
  width: 100%;
  color: #348599;
  font-size: 16px;
  font-weight: bold;
  font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;
  background-color: #fff;
  align-items: center;
  justify-content: center;
`

const Container = ({ uiStore, content }) => (
  <Wrapper>
    {content}
    {uiStore.user.name}
    <FormattedMessage
      id="app.title"
      defaultMessage="Welcome to {what}"
      description="Welcome header on app main page"
      values={{ what: 'react-intl' }}
    />
    <FormattedHTMLMessage
      id="app.intro"
      defaultMessage="To get started, edit <code>src/App.js</code> and save to reload."
      description="Text on main page"
    />
  </Wrapper>
)

export default inject('uiStore')(observer(Container))
