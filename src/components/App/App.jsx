import React from 'react'
import { Provider } from 'mobx-react'
import styled from 'styled-components'
import { createBrowserHistory } from 'history'
import { addLocaleData, IntlProvider } from 'react-intl'
import localeEn from 'react-intl/locale-data/en'
import localeES from 'react-intl/locale-data/es'
import Router from '../../router/router'
import { createStores } from '../../stores/createStore'
import UserModel from '../../models/UserModel'
import messagesES from '../../translations/es.json'
import messagesEN from '../../translations/en.json'

const messages = {
  es: messagesES,
  en: messagesEN
}
addLocaleData([...localeEn, ...localeES])

const language = 'es'

const Container = styled.div`
  width: 100%;
  height: 100%;
`

const history = createBrowserHistory()
const defautlUser = UserModel.create({
  name: 'Default Name'
})
const stores = createStores(history, defautlUser)

const App = () => (
  <IntlProvider locale={language} messages={messages[language]}>
    <Provider {...stores}>
      <Container>
        <Router history={history} />
      </Container>
    </Provider>
  </IntlProvider>
)

export default App
